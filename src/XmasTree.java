import java.util.Scanner;

public class XmasTree {

    private int maxlevel;
    private final static String welcome = " ___________________________________ \n" +
            "/  !Merry Christmas!                \\\n" +
            "|                                   |\n" +
            "| How tall do you want your         |\n" +
            "\\ Christmas tree?                  /\n" +
            " ----------------------------------- \n" +
            "   \\\n" +
            "    \\\n" +
            "        .--.\n" +
            "       |o_o |\n" +
            "       |:_/ |\n" +
            "      //   \\ \\\n" +
            "     (|     | )\n" +
            "    /'\\_   _/`\\\n" +
            "    \\___)=(___/\n";


    /**
     * If a number is passed as an argument the program will create a christmas tree with that height.
     * If nothing is passed, then Tux will ask you about it's height. Type 0 to exit the program.
     * @param args height of the Tree
     */
    public static void main(String args[]) {

        // Check if interactive mode or tree level argument was passed
        if (args.length != 0) {
            XmasTree tree = new XmasTree(Integer.parseInt(args[0]));
            System.out.println(tree.toString());
        } else {

            int level;
            do {
                // Print Welcome Message
                System.out.println(welcome);

                // Get the tree's height
                Scanner in = new Scanner(System.in);
                level = in.nextInt();

                // Create tree, print and repeat
                XmasTree tree = new XmasTree(level);
                System.out.println(tree.toString());

            } while (level != 0);

        }

    }

    /**
     * Constructor for the XMasTree
     * @param level maximal height of the tree
     */
    private XmasTree(int level) {
        this.maxlevel = level;
    }

    /**
     * Creates the ASCII representation of the tree
     * @return Tree
     */
    public String toString() {
        String result = "";

        // Create Top, then loop through all levels and add trunk at the end
        result = result + this.generateTreeTopper();
        for (int i =1; i<=this.maxlevel;i++) {
            result = result + this.generateLevel(i);
        }
        result = result + this.generateTrunk();

        return result;
    }

    /**
     * Generates a tree's branch (level)
     * @param level level count from top of the tree
     * @return Trees level with spaces for formatting
     */
    private String generateLevel(int level) {
        return generateSpaces(level, true) + generateUpperRow(level) + "\n"
                + generateSpaces(level, false) + generateLowerRow(level) + "\n";
    }

    /**
     * Generates the upper row of a level
     * @param level level count from top of the tree
     * @return upper row
     */
    private String generateUpperRow(int level) {
        String result = "/";

        for (int i = 1; i < level; i++) {
            result = result + ".,";
        }
        result = result + ".\\";

        return result;
    }

    /**
     * Generates the lower row of a level
     * @param level level count from top of the tree
     * @return lower row
     */
    private String generateLowerRow(int level) {
        String result = "/";

        for (int i = 1; i <= level; i++) {
            result = result + ",.";
        }
        result = result + ",\\";

        return result;
    }

    /**
     * Generates the right amount of spaces needed for a row
     * @param level level count from top of the tree, 0 for tree's star
     * @param upper is it the upper row of a level?
     * @return String of spaces
     */
    private String generateSpaces(int level, boolean upper) {
        int amount;

        if (level == 0) {
            amount = this.maxlevel + 1;
        } else {
            int extra = 0;
            if (upper) {
                extra = 1;
            }
            amount = this.maxlevel - level + extra;
        }

        String spaces = "";
        for (int i = 0; i < amount; i++) {
            spaces = spaces + " ";
        }

        return spaces;
    }

    /**
     * Generates the tree topper star
     * @return Star on top of the tree
     */
    private String generateTreeTopper() {
        String treeTopper = "*";
        return generateSpaces(0, false) + treeTopper + "\n";
    }

    /**
     * Generates the tree's stump
     * @return Tree's stump
     */
    private String generateTrunk() {
        int amountCharacters = 5 + (this.maxlevel - 1) * 2;
        String result = "";

        for (int i = 0; i< (amountCharacters-3)/2; i++) {
            result = result + "^";
        }
        String trunk = "[_]";
        result = result + trunk;
        for (int i = 0; i< (amountCharacters-3)/2; i++) {
            result = result + "^";
        }

        return result;
    }

}