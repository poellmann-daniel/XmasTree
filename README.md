# X-mas tree for your terminal

This little Java Program will output a Christmas Tree in your terminal. This was a 
coding exercise for my university but I really liked the concept and decided to publish it.

## Installation
Compile it:
`javac XmasTree.java`

## Dependencies
XmasTree relies obviously on `JAVA`. I'm not sure about which version exactly but I guess all widely used ones should work.
 
## Usage
Interactive mode (you'll be greeted and have to type in the height you want the tree to be)

`java XmasTree`

Just the tree: `java XmasTree 5`

## Examples
`java XmasTree 5`

```
      *
     /.\
    /,.,\
    /.,.\
   /,.,.,\
   /.,.,.\
  /,.,.,.,\
  /.,.,.,.\
 /,.,.,.,.,\
 /.,.,.,.,.\
/,.,.,.,.,.,\
^^^^^[_]^^^^^
```

## Licence
DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE, see `LICENCE`